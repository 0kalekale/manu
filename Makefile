include config.mk

SRC = src/manu.c

all: 
	mkdir -p build
	$(CC) $(SRC) -o build/manu $(CFLAGS) 

install: all
	mkdir -p $(PREFIX)/bin
	cp -f $(BIN) $(PREFIX)/bin/manu

uninstall:
	rm -f $(PREFIX)/bin/manu

clean:
	rm -rf build

.PHONY:
	all install uninstall clean
