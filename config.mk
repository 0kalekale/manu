PREFIX = /usr/local

CC = gcc
LD = $(CC)
CFLAGS   = -Wextra -Wall -Os -std=gnu11
LDFLAGS  = -s -static
