/* See LICENSE file for copyright and license details. */

static char *const rcinitcmd[]     = { "/etc/manu/init", NULL };
static char *const rcrebootcmd[]   = { "/etc/manu/shutdown", "reboot", NULL };
static char *const rcpoweroffcmd[] = { "/etc/manu/shutdown", "poweroff", NULL };
