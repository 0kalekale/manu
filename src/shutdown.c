/*
	shutdown.c: /bin/shutdown binary
	copyright (C) 2023 0kalekale
    	This program is free software: you can redistribute it and/or modify
    	it under the terms of the GNU General Public License as published by
    	the Free Software Foundation, either version 3 of the License, or
    	(at your option) any later version.

    	This program is distributed in the hope that it will be useful,
    	but WITHOUT ANY WARRANTY; without even the implied warranty of
    	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

    	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define _POSIX_SOURCE
#include <linux/reboot.h>
#include <sys/reboot.h>  
#include <sys/syscall.h>   
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/signal.h>
#include <string.h>

int main(int argc, char *argv[]) {

	/* 
	 *	$ shutdown 
	 *	sends the shutdown signal to manu 
	 * 	manu runs the `/etc/manu/shutdown` script 
	 */
	if (argc<2) {
		kill(1, SIGUSR1);
		// *DANGER* if the shutdown script doesnt call sync()
		// cached fs data not written to disk will be lost 
		return reboot(LINUX_REBOOT_CMD_POWER_OFF);
	}
	/*
	 *      $ shutdown -r 
	 *	sends the reboot signal to manu
	 * 	manu runs the `/etc/manu/shutdown reboot` script 
         */
	else if (strcmp(argv[1], "-r")) { 
		kill(1, SIGINT);
		// *DANGER* if the shutdown script doesnt call sync()
		// cached fs data not written to disk will be lost
		return reboot(LINUX_REBOOT_CMD_RESTART);
	}
	// should never see the light of the day
	else {
		printf("manu-shutdown: invalid option");
		return 127;
	}
}
